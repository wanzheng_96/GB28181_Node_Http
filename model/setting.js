let db = require('../db/dbmanager');

/**
 * 设置信息
 */
let Setting={
    cache:null,
    /**
     * @param {*} data 保存数据
     */
    save:function(newdata){
        let me = this;
        me.cache = newdata;

        let p = new Promise(function(resolve,reject){
            me.load().then((dbData)=>{
                if(dbData){
                    me.update(newdata);
                }else{
                    me.insert(newdata);
                }
                resolve(newdata);
            });
        });
        return p;
    },
    /**
     * 插入数据
     */
    insert:function(newdata){
        let sqldata={
            $sip_command_account  : newdata.sip_command_account,
            $sip_command_password : newdata.sip_command_password,
            $sip_command_host     : newdata.sip_command_host,
            $server_realm         : newdata.server_realm,
            $sip_command_port     : newdata.sip_command_port,
            $keep_alive_second    : newdata.keep_alive_second,
            $sip_rtp_port         : newdata.sip_rtp_port,
            $server_hls_port      : newdata.server_hls_port,
            $snap_path            : newdata.snap_path
        };
        let p=new Promise(function(resolve,reject){
            const sql = "insert into SETTING(sip_command_account,sip_command_password,sip_command_host,server_realm,sip_command_port,keep_alive_second,sip_rtp_port,server_hls_port,snap_path"
             +
                ") values(" +
                "$sip_command_account,$sip_command_password,$sip_command_host,$server_realm,$sip_command_port,$keep_alive_second,$sip_rtp_port,$server_hls_port,$snap_path)";
            db.sqliteDB.run(sql,sqldata);
            resolve(newdata);
        });
        return p;
    },
    /**
     * 更新
     */
    update:function(newdata){
        let me=this;
        let sqldata={
            $sip_command_account  : newdata.sip_command_account,
            $sip_command_password : newdata.sip_command_password,
            $sip_command_host     : newdata.sip_command_host,
            $sip_command_port     : newdata.sip_command_port,
            $server_realm         : newdata.server_realm,
            $sip_rtp_host         : newdata.sip_rtp_host,
            $sip_rtp_port         : newdata.sip_rtp_port,
            $server_hls_port      : newdata.server_hls_port,
            $keep_alive_second    : newdata.keep_alive_second,
            $snap_path            : newdata.snap_path
        };
        let p = new Promise(function(resolve,reject){
            const sql = "update SETTING set " + 
                "sip_command_account = $sip_command_account,sip_command_password= $sip_command_password," +
                "sip_command_host=$sip_command_host,server_realm=$server_realm,sip_command_port=$sip_command_port,keep_alive_second=$keep_alive_second," +
                "sip_rtp_port=$sip_rtp_port,"+
                "sip_rtp_host=$sip_rtp_host,"+
                "snap_path=$snap_path,"+
                "server_hls_port=$server_hls_port" +
                " where id=1";
            db.sqliteDB.run(sql,sqldata);
            me.cache=newdata
            resolve(newdata);
        });
        return p;
    },
    /**
     * 加载设置
     */
    load:function(){
        let me=this;
        let p =new Promise(function(resolve,reject){
            const sql="SELECT * FROM SETTING WHERE id=1";
            db.sqliteDB.oneData(sql,(data)=>{

                if(!data || data.length==0){
                    let newdata={
                        sip_command_account  : '34010300002000002250',
                        sip_command_password : '12345678',
                        sip_command_host     : 'localhost',
                        sip_command_port     : 5060,
                        server_realm         : '3401030000',
                        keep_alive_second    : 60,
                        snap_path            : '',
                        sip_rtp_port         : 9092,
                        sip_rtp_host         : 'localhost',
                        server_hls_port      : 9094                    };
                    return me.insert(newdata)
                }
                else{
                    me.cache=data
                    resolve(data);
                }
            });
        });
        return p;
    }
};

module.exports=Setting;