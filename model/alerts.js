let db = require('../db/dbmanager');
const UUID = require('uuid');
const constants = require('../data/constants');
var format          = require('string-format');
/**
 * 事件截图信息表
 */
let Alerts={
    get:function(id){
        let sql = "SELECT * FROM ALERTS WHERE id=$id";
        let param = {$id:id};
        let p = new Promise(function(resolve,reject){
            db.sqliteDB.all(sql,param,function(err,row){
                if(err){
                    reject(err);
                }else{
                    resolve(row[0]);
                }
            });
        });
        return p;
    },
    insert:function(deviceid, channelid, thumb, picture, add_time){
        let p = new Promise(function(resolve, reject){
            let sql   = "INSERT INTO ALERTS(id,add_time,deviceid,channelid,thumb,picture) VALUES ($id, $add_time,$deviceid, $channelid, $thumb, $picture)";
            let key = UUID.v1().replace(/-/g,'');

            let param = {$id: key, $add_time: add_time, $deviceid:deviceid,$channelid:channelid, $thumb:thumb, $picture:picture};
            db.sqliteDB.run(sql, param);
            resolve(key);
        });
        return p;
    },
    // 某时间段是否已经有了截图
    exists:function(deviceid, channelid, add_time){
        let sql = "SELECT * FROM ALERTS WHERE deviceid=$deviceid AND channelid=$channelid AND add_time>$add_time";
        let param = {$deviceid:deviceid, $channelid:channelid, $add_time:add_time-constants.alertInterval * 1000};
        let p = new Promise(function(resolve,reject){
            db.sqliteDB.all(sql,param,function(err,row){
                if(err){
                    reject(err);
                }else{
                    resolve(row);
                }
            });
        });
        return p;

    },
    // 插入前检查
    insertCheck:async function(deviceid,channelid, thumb, picture, add_time){
        let exist = await Alerts.exists(deviceid, channelid, add_time);
        if(exist && exist.length>0){
            console.warn('短时间重复触发，不写入数据库');
            // 已经存在
            let p = new Promise(function(resolve,reject){
                reject(false);
            });
            return p;
        }else{
            return Alerts.insert(deviceid,channelid,thumb,picture,add_time);
        }
    },
    /**
     * 查询列表
     * @param {*} page 第几页
     * @param {*} size 每页大小
     */
    list: function(page, size, deviceid,channelid){
        let p = new Promise(function(resolve,reject){
            let sql = "SELECT * FROM ALERTS ";
            let data;
            if(deviceid){
                sql += " WHERE deviceid = ? AND channelid = ?";
                data = [deviceid, channelid];
            }else{
                data = [];
            }
            sql += format(" order by add_time DESC limit {0} offset {0}*{1}", size, page);
            db.sqliteDB.all(sql, data, function(err, rows){
                resolve(rows);
            });
        });
        return p;
    },
    count: function(deviceid, channelid){
        let p = new Promise(function(resolve,reject){
            let sql = "SELECT COUNT(*) as c FROM ALERTS ";
            let data;
            if(deviceid){
                sql += " WHERE deviceid = ? AND channelid = ?";
                data = [deviceid, channelid];
            }else{
                data = [];
            }
            db.sqliteDB.all(sql,data, (err, ret)=>{
                resolve(ret[0].c)
            });
        });
        return p;
    },
    del:function(id){
        let param={
            $id: id
        };
        const sql = "DELETE FROM ALERTS WHERE id=$id";
        db.sqliteDB.run(sql,param);

    }
};

module.exports = Alerts;