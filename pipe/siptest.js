const digest        = require('sip/digest');

let username = '"34020000001320000047"';
let password = "12345678";
let realm = '"3402000000"';
let nonce = '959f6f86dc5d2c88beeb10111447b0c2';
let cnonce = '0a4f113b';
let method = 'REGISTER';
let uri = 'sip:34020000002000000001@3402000000';
let qop = 'auth';
let nc = '00000001';
let response = '"a5f201d65930668f3e3097a2aeb33673"';

let rq={
    method: method,
    headers: {
      authorization: [
        {
            scheme: 'Digest',
            username: username,
            realm: realm,
            nonce: nonce,
            uri: uri,
            response: response,
            cnonce: cnonce,
            qop: qop,
            nc: nc
          }
      ]
    },
    content: ''
  };

if(rq && rq.headers && rq.headers.authorization){
    rq.headers.authorization[0].username = trim(rq.headers.authorization[0].username);
    rq.headers.authorization[0].realm = trim(rq.headers.authorization[0].realm);
    rq.headers.authorization[0].nonce = trim(rq.headers.authorization[0].nonce);
    rq.headers.authorization[0].uri = trim(rq.headers.authorization[0].uri);
    rq.headers.authorization[0].response = trim(rq.headers.authorization[0].response);
    rq.headers.authorization[0].cnonce = trim(rq.headers.authorization[0].cnonce);
}

function trim(input){
    return (input||'').replace(/\"/g,'');
};

let ha1 = digest.kd(trim(username),trim(realm),password);
let ha2 = digest.kd(method, uri);
console.log('ha1',ha1,'ha2=',ha2);

let res = digest.kd(ha1,nonce,nc,cnonce,qop,ha2);
console.log('带nonce的response',res);

let res0=digest.kd(ha1,nonce,ha2);
console.log('不带nonce的response',res0);
console.log('期待结果', response);

let session = {
    realm:trim(realm),
    nonce:trim(rq.headers.authorization[0].nonce)
};

console.log(rq.headers);
let check = digest.authenticateRequest(session, rq, {user: username, password: password});
console.log('sip module 校验结果',check);