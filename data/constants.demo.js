const fs   = require('fs');
const http = require('http');

/**
 * 常量设置
 */
module.exports = {
    // api版本
    'version':'/api/v1/',
    /**
     * http接口状态码
     */
    'httpCode':{
        'OK'            : {code: 20000, message: '操作成功'},
        'ILLEGAL_TOKEN' : {code: 50008, message: '无效凭据'},
        'OTHER_LOGIN'   : {code: 50012, message: '已登陆'},
        'TOKEN_EXPIRE'  : {code: 50014, message: '登陆过期'},
        'PASS_ERROR'    : {code: 60204, message: '密码错误'},
        'ILLEGAL_PARAM' : {code: 60205, message: '无效参数'},
        'NO_DATA'       : {code: 60206, message: '未找到数据'},
        'DUPLITED'      : {code: 60207, message: '数据已经存在'}
    },
    // sqlite 数据库位置
    'dbPath':'./data/cameras.db',

    // 报警图片文件夹，程序监控此文件的变化
    'alertBasePath':'/www/wwwroot/GB28181_Node_Http/public/static/img/capture/',
    'fileRegex':{
        'hikvision':{
            regex:/[\d|\.]+(_01)?_(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})(\d{3})_([A-Z_]*).jpg/,
            regex_name:['file',null,'year','month','day','hour','minute','second','ms','type']            
        }
    },
    // 摄像机上传的图片分类存放到下面地址
    'cameraBak':{
        'savePath':{
            'AI_OPEN_PLAT':'/www/wwwroot/GB28181_Node_Http/public/static/img/alerts/',
            'TIMING':'/www/wwwroot/GB28181_Node_Http/public/static/img/timing/'
        },
        'url':{
            'AI_OPEN_PLAT':'/static/img/alerts/',
            'TIMING':'/static/img/timing/'
        }
    },
    // 几秒内重复报警忽略
    'alertInterval':20,
    'http': {
        'port'          : 7000
    },
    'defaultAdmin':{
        'username': 'admin',
        'password': '123456'
    },
    'sip': {
        'version'         : '2.0',
        'udp'             : true,
        'tcp'             : true,
        'useragent'       : 'NODE GB28181 SERVER V1',
        // 是否鉴权
        'authorize'       : true
    },
    'logger'              : {
        'filecount': 5,   // 日志文件保存数量
        'path'    : 'logs/'  // GB日志默认存储位置，注意ZLMediaKit的日志位置不是在这里设置的
    },
    /**
     * 在连的设备信息，并不一定与数据库同步,格式
     *  online:true,
        session:{realm:me.sipSetting.server_realm},
        version: sip版本号
        sumnum: 通道数
        password  : sip_command_password,
        deviceid  : deviceid,
        session   : {
          realm   : server_realm
        },
        sip_command_host  : uriObj.sip_command_host,
        sip_command_port  : uriObj.sip_command_port,
        play      : false, // 是否在播放     
        uri       : uriObj.uri,
        last_heart:时间戳
     */
    registry:{
        invites:{}
    },
    // 媒体服务器信息
    mediaservers:{},
    // 程序运行开始时间
    start:new Date(),
    // 把registry保存到文件
    save:function(callback){
        let cache = [];
        // 删除invites信息
        if(this.registry.data){
            delete this.registry.data['invites'];
        }
        
        let str = JSON.stringify(this.registry,function(key,value){
            if (typeof value === 'object' && value !== null) {
                if (cache.indexOf(value) !== -1) {
                    return;
                }
                cache.push(value);
            }
            return value;
        });
        fs.writeFile('./data/registry.data',str,callback);
    },
    // 从文件加载registry，就是上次程序关闭时的设置
    load:function(callback){
        let me=this;
        fs.readFile('./data/registry.data',(error,data)=>{
            if(error){
                callback(false);
            }else{
                me.registry = JSON.parse(data.toString());
                callback(true);
            }
        });
    },
    /**
     * 获取公网ip
     */
    ip: null,
    ipApi:'http://ip.taobao.com/outGetIpInfo?ip=myip&accessKey=alibaba-inc',
    publicIp:function(){
        const me = this;
        let p = new Promise(function(resolve, reject){
            if(me.ip){
                resolve(me.ip)
            }else{
                const url = me.ipApi;

                http.get(url ,(res)=>{
                    var html="";
                    res.on("data",(data)=>{
                        html += data;
                    });
                    res.on("end",()=>{
                        try{
                            const ret = JSON.parse(html);
                            if(ret.code === 0){
                                me.ip = ret.data.ip;
                                resolve(me.ip);
                            }else{
                                console.error('获取本地IP地址时，访问接口报错', ret);
                                reject();
                            }
                        }catch(e){
                            console.error('获取本地IP地址时，访问接口报错');
                            reject();
                        }
                    });
                }).on('error',(e)=>{
                    console.error('constants 117 line:', e);
                    reject();
                });
            }
        });
        return p;
    },
    rstring:function() { return Math.floor(Math.random()*1e6).toString(); }
}