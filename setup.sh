echo "1. 编译前端程序并拷贝到 public/ 目录"
cd ./CameraUI
npm i --registry=https://registry.npm.taobao.org
npm run build:prod
echo "2. 安装后端程序依赖"
cd ..
npm i --registry=https://registry.npm.taobao.org
echo "3. 安装 apidoc 工具"
npm install apidoc -g --registry=https://registry.npm.taobao.org
echo "4. 生成api文档并拷贝到 public/ 目录"
apidoc -i routers/ -o public/apidoc/
echo "5. 设置可执行文件权限"
chmod +x ZLMediaKit/MediaServer

echo "安装"
cd /usr/local/src/
wget http://ftp.icm.edu.pl/pub/unix/graphics/GraphicsMagick/1.3/GraphicsMagick-1.3.21.tar.gz
tar zxvf GraphicsMagick-1.3.21.tar.gz
cd GraphicsMagick-1.3.21
./configure
make -j8
make install
gm version

echo " *******************************************"
echo " * 安装完成，可以使用 node app.js 命令启动程序"
echo " * 或者使用pm2 start app.js 作为服务启动程序"