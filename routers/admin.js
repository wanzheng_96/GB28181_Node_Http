const express   = require('express');
const constants = require('../data/constants')
const router    = express.Router();
const adminController  = require('../controller/v1/admin');

/**
* @api {post} /admin/checklogin 用户登录
* @apiDescription 用户登录
* @apiName AdminLogin
* @apiGroup 管理员接口
* @apiParam {string} username 用户名
* @apiParam {string} password 密码
* @apiVersion 1.0.0
*/
router.post(constants.version + 'admin/checklogin' ,adminController.checklogin);

/**
 * @api {post} /admin/logout 用户注销登陆
 * @apiDescription 用户注销登陆
 * @apiName AdminLogout
 * @apiGroup 管理员接口
 * @apiVersion 1.0.0
 */
router.post(constants.version + 'admin/logout',adminController.logout);

/**
 * @api {get} /admin/info 获取用户信息
 * @apiDescription 获取用户信息
 * @apiName AdminInfo
 * @apiGroup 管理员接口
 * @apiVersion 1.0.0
 */
router.get(constants.version  + 'admin/info'  ,adminController.info);
router.post(constants.version + 'admin/changePass' ,adminController.changePass);
module.exports = router;