const express = require('express');
const router = express.Router();
const constants = require('../data/constants')

const channelController = require('../controller/v1/channel');

/**
 * @api {get} /channel/list 查询设备列表
 * @apiDescription 查询设备列表
 * @apiName ChannelList
 * @apiGroup 设备管理
 * @apiVersion 1.0.0
 */
router.get(constants.version + 'channel/list', channelController.list);

/**
 * @api {get} /channel/bye 停止播放
 * @apiDescription 停止播放
 * @apiName ChannelBye
 * @apiGroup 设备管理
 * @apiVersion 1.0.0
 */
router.get(constants.version + 'channel/bye', channelController.bye);
/**
 * @api {get} /channel/snap 截屏
 * @apiDescription 截屏
 * @apiName ChannelBye
 * @apiGroup 设备管理
 * @apiVersion 1.0.0
 */
router.get(constants.version + 'channel/snap', channelController.snap);

/**
 * @api {get} /channel/registry 打印连接信息
 * @apiDescription 打印连接信息
 * @apiName ChannelBye
 * @apiGroup 设备管理
 * @apiVersion 1.0.0
 */
router.get(constants.version + 'channel/registry', channelController.registry);


module.exports = router;