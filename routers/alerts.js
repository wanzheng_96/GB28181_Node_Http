const express = require('express');
const router = express.Router();
const constants = require('../data/constants')

const alertsController = require('../controller/v1/alerts');

/**
 * @api {get} /alerts/list 报警信息列表
 * @apiDescription 报警信息列表
 * @apiName AlertsList
 * @apiGroup 报警管理
 * @apiVersion 1.0.0
 */
router.get(constants.version + 'alerts/list', alertsController.list);
/**
 * @api {get} /alerts/remove 删除报警信息
 * @apiDescription 删除报警信息
 * @apiName AlertsRemove
 * @apiGroup 报警管理
 * @apiVersion 1.0.0
 */
router.get(constants.version + 'alerts/del', alertsController.del);

module.exports = router;