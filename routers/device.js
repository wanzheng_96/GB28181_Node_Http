const express = require('express');
const router = express.Router();
const constants = require('../data/constants')

const deviceController = require('../controller/v1/device');

/**
 * @api {get} /device/list 查询设备列表
 * @apiDescription 查询设备列表
 * @apiName DeviceList
 * @apiGroup 设备管理
 * @apiVersion 1.0.0
 */
router.get(constants.version + 'device/list', deviceController.list);

/**
 * @api {get} /device/query 查询某个设备
 * @apiDescription 查询某个设备
 * @apiName DeviceQuery
 * @apiGroup 设备管理
 * @apiVersion 1.0.0
 */
router.get(constants.version + 'device/query', deviceController.query);

/**
 * @api {get} /device/catalog 请求更新目录
 * @apiDescription 请求更新目录
 * @apiName DeviceCatalog
 * @apiGroup 设备管理
 * @apiVersion 1.0.0
 */
router.get(constants.version + 'device/catalog', deviceController.catalog);

/**
 * @api {post} /device/deldevice 删除设备
 * @apiDescription 删除设备
 * @apiName DelDevice
 * @apiGroup 设备管理
 * @apiVersion 1.0.0
 */
router.post(constants.version + 'device/deldevice', deviceController.deldevice);

/**
 * @api {post} /device/updatename 更新通道名称
 * @apiDescription 更新通道名称
 * @apiName UpdateName
 * @apiGroup 设备管理
 * @apiVersion 1.0.0
 */
router.post(constants.version + 'device/updatename', deviceController.updatename);

/**
 * @api {post} /device/update 更新
 * @apiDescription 更新
 * @apiName update
 * @apiGroup 设备管理
 * @apiVersion 1.0.0
 */
router.post(constants.version + 'device/update', deviceController.update);

module.exports = router;