/**
 * SQLite3 操作封装
 * Author: xundh
 * 参考文档 https://www.sqlitetutorial.net/sqlite-nodejs/query/
 */
 
var fs = require('fs');
var sqlite3 = require('sqlite3').verbose();
 
var DB = DB || {};
const log4js      = require('../data/log');
const logger      = log4js.getLogger('db');

DB.SqliteDB = function(file){
    logger.log('创建sqlite3对象');
    DB.db = new sqlite3.Database(file);
 
    DB.exist = fs.existsSync(file);
    if(!DB.exist){
        logger.info("创建数据库文件");
        fs.openSync(file, 'w');
    };
};

DB.error = function(err){
    logger.trace("Error Message:" + err.message + " ErrorNumber:" + err.errno);
};
 
/**
 * 创建表
 * @param {string} sql create table的建表语句
 */
DB.SqliteDB.prototype.createTable = function(sql){
    DB.db.serialize(function(){
        DB.db.run(sql, function(err){
            if(null != err){
                DB.error(err);
                return;
            }
        });
    });
};

/**
 * 普通查询，查询一条
 * @param {*} sql 查询语句
 * @param {*} callback 
 */
DB.SqliteDB.prototype.oneData = function(sql, callback){
    DB.db.get(sql, function(err, rows){
        if(null != err){
            DB.error(err);
            return;
        }
 
        if(callback){
            callback(rows);
        }
    });
};

/**
 * 普通查询
 * @param {*} sql 查询语句
 * @param {*} callback 
 */
DB.SqliteDB.prototype.queryData = function(sql, callback){
    DB.db.all(sql, function(err, rows){
        if(null != err){
            DB.error(err);
            return;
        }
 
        if(callback){
            callback(rows);
        }
    });
};

/**
 * 带参数查询，查不到的话不向下执行
 * @param {*} sql 
 * @param {*} data 
 * @param {*} callback 
 */
DB.SqliteDB.prototype.each = function(sql,data, callback){
    DB.db.each(sql, data, function(err, rows){
        if(callback){
            callback(err, rows);
        }
    });
};

/**
 * 带参数查询
 * @param {*} sql 
 * @param {*} data 
 * @param {*} callback 
 */
DB.SqliteDB.prototype.all = function(sql, data, callback){
    DB.db.all(sql, data, function(err, rows){
        if(callback){
            callback(err, rows);
        }
    });
};

/**
 * 获取最后一次的自增主键值
 * @param {*} callback 
 */
DB.SqliteDB.prototype.lastRow = function(table, callback){
    const sql = "SELECT max(last_insert_rowid()) FROM " + table ;
    this.queryData(sql, callback);
};

/**
 * 执行带参数的SQL语句，用于update,insert等
 * @param {*} sql 
 * @param {*} data 
 */

DB.SqliteDB.prototype.run = function(sql, data){
     DB.db.run(sql,data);
};

/**
 * 关闭数据库
 */
DB.SqliteDB.prototype.close = function(){
    DB.db.close();
};

exports.SqliteDB = DB.SqliteDB;