const constants   = require('../data/constants');
const SqliteDB    = require('./SqliteDB').SqliteDB;
const log4js      = require('../data/log');
const logger      = log4js.getLogger('db');

let DbManager={
    /**
     * 打开数据库连接
     */
    open:function(){
        if(this.sqliteDB){
            logger.warn('数据库已经打开连接');
        }
        else{
            logger.info('连接数据库');
            this.sqliteDB = new SqliteDB(constants.dbPath);
            DbManager.init();
        }
    },
    /**
     * 初始化表
     */
    init:function(){
        // 管理员表
        const admin   = "create table if not exists ADMIN(id INTEGER PRIMARY KEY AUTOINCREMENT, username varchar(64),password varchar(64))";
        this.sqliteDB.createTable(admin);
        // 摄像机或NVR表
        const device  = "create table if not exists DEVICE(id INTEGER PRIMARY KEY AUTOINCREMENT, deviceid varchar(64),online INTEGER,password varchar(64),keep_alive INTEGER,sumnum INTEGER,sip_command_host varchar(256),sip_command_port INTEGER,version varchar(8),last_heart INTEGER,device_type varchar(8),device_name varchar(64),server_id INTEGER,rtsp_url varchar(256),protocol varchar(16))";
        this.sqliteDB.createTable(device);
        // 通道/设备表
        const channel = "create table if not exists CHANNEL(id INTEGER PRIMARY KEY AUTOINCREMENT, parentid varchar(64), deviceid varchar(64),online INTEGER,keep_alive INTEGER,name varchar(256),manufacture varchar(256),model varchar(256),owner varchar(256),civilcode varchar(256),address varchar(256),snap varchar(256),registerway INTEGER,secrecy INTEGER,last_heart INTEGER,rtsp_url varchar(256))";
        this.sqliteDB.createTable(channel);
        // 媒体服务器
        const mediaserver="create table if not exists MEDIASERVER(id INTEGER PRIMARY KEY AUTOINCREMENT,server_id varchar(64), media_host varchar(128), http_port INTEGER,https_port INTEGER, rtmp_port INTEGER, rtp_min_port, rtp_max_port, secret varchar(64))";
        this.sqliteDB.createTable(mediaserver);        
        // 设置表
        const setting = "create table if not exists SETTING(id INTEGER PRIMARY KEY AUTOINCREMENT, sip_command_account text,sip_command_password text,sip_command_host text,sip_command_port INTEGER, sip_rtp_port INTEGER, sip_rtp_host varchar(256), server_realm text,keep_alive_second INTEGER,server_hls_port INTEGER,snap_path VARCHAR(256))";
        this.sqliteDB.createTable(setting);
        // 报警表
        const alerts  = "create table if not exists ALERTS(id VARCHAR(64) PRIMARY KEY, add_time INTEGER(13), deviceid varchar(64), channelid varchar(64), thumb varchar(512), picture varchar(512))";
        this.sqliteDB.createTable(alerts);
        // 定时截图
        const timing  = "create table if not exists TIMING(id VARCHAR(64) PRIMARY KEY, add_time INTEGER(13), deviceid varchar(64), channelid varchar(64), thumb varchar(512), picture varchar(512))";
        this.sqliteDB.createTable(timing);
    },
    /**
     * 关闭数据库连接
     */
    close:function(){
        if(this.sqliteDB){
            this.sqliteDB.close();
            this.sqliteDB=null;
        }
    },
    sqliteDB:null
};
module.exports = DbManager;