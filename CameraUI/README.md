# 监控WEB端

--------------------

## 一、 开发资料

文件夹CameraUI里是前端源代码，使用Element-UI。通过下面的命令调试：

```bash
cd GB28181_NODE_HTTP/CameraUI
# 安装依赖
npm install
# 预览
npm run dev
# 编译输出，输出到public目录
npm run build:prod
```

## 二、 相关技术框架

- vue
- [vue admin template](https://github.com/PanJiaChen/vue-admin-template)
- 视频播放器 [flv.js](https://github.com/bilibili/flv.js)
- echarts 图表组件
