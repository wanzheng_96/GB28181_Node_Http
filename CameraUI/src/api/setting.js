import request from '@/utils/request'

/**
 * 一些sip的设置项放到服务器上
 */

export function save(data){
    return request({
      url: '/v1/setting/save',
      method:'post',
      data
    })
}
export function load(){
    return request({
      url: '/v1/setting/load',
      method: 'get'
    })
}
