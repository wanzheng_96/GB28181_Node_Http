import request from '@/utils/request'

export function list(data){
  return request({
    url: '/v1/alerts/list',
    method: 'get',
    params: data
  })
}
export function del(data){
  return request({
    url: '/v1/alerts/del',
    method: 'get',
    params: data
  })
}