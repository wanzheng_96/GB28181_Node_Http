import request from '@/utils/request'


export function list(data){
  return request({
    url: '/v1/timing/list',
    method: 'get',
    params: data
  })
}
