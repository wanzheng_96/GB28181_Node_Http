import request from '@/utils/request'

export function checklogin(data) {
  return request({
    url: '/v1/admin/checklogin',
    method: 'post',
    data
  })
}

export function getInfo(token) {
  return request({
    url: '/v1/admin/info',
    method: 'get',
    params: { token }
  })
}

export function logout() {
  return request({
    url: '/v1/admin/logout',
    method: 'post'
  })
}

export function changePass(data) {
  return request({
    url: '/v1/admin/changePass',
    method: 'post',
    data
  })
}
