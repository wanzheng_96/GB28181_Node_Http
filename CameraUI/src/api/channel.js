import request from '@/utils/request'

/**
 * 获取通道列表
 * @param {Object} data
 */
export function list(data) {
  return request({
    url: '/v1/channel/list',
    method: 'get',
    params: data
  })
}
// export function captures(data){
//   return request({
//     url: '/v1/channel/captures',
//     method: 'get',
//     params: data
//   })
// }
// export function alerts(data){
//   return request({
//     url: '/v1/channel/alerts',
//     method: 'get',
//     params: data
//   })
// }