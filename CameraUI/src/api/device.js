import request from '@/utils/request'

/**
 * 获取设备列表
 * @param {Object} data
 */
export function list(data){
  return request({
    url: '/v1/device/list',
    method: 'get',
    params: data
  })
}
/**
 * 请求更新目录
 * @param {Object} data
 */
export function catalog(data){
  return request({
    url: '/v1/device/catalog',
    method: 'get',
    params : data
  })
}

export function deldevice(data){
  return request({url: '/v1/device/deldevice', method:'POST', data});
}
/**
 * @param {Object} data 更新通道名称
 */
export function updatename(data){
  return request({url: '/v1/device/updatename', method:'POST', data})
}
/**
 * 
 * @param {*} data 更新媒体服务器
 */
export function update(data){
  return request({url: '/v1/device/update', method:'POST', data})
}
