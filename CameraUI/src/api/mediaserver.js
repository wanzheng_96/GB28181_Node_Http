import request from '@/utils/request'

/**
 * 获取媒体服务器列表
 * @param {Object} data
 */
export function list(data){
  return request({
    url: '/v1/mediaserver/list',
    method: 'get',
    params: data
  })
}
/**
 * 删除一个媒体服务器
 * @param {*} data 
 */
export function del(data){
    return request({
      url: '/v1/mediaserver/del',
      method:'POST',
      data
    })
}
/**
 * @param {Object} data更新媒体服务器
 */
export function update(data){
 return request({
   url: '/v1/mediaserver/update',
   method:'POST',
   data
 })
}
/**
 * @param {Object} data查询媒体服务器
 */
export function query(data){
    return request({
      url: '/v1/mediaserver/query',
      method:'POST',
      data
    })
}
   
/**
 * @param {Object} data插入媒体服务器
 */
export function insert(data){
    return request({
      url: '/v1/mediaserver/insert',
      method:'POST',
      data
    })
}