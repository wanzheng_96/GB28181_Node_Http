import request from '@/utils/request'

/**
 * 获取媒体服务器信息
 * @param {Object} data
 */
export function getServerInfo(data) {
  return request({
    url: '/v1/media/getServerConfig',
    method: 'get',
    params: data
  })
}
