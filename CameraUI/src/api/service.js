import request from '@/utils/request'

/**
 * 改变gb28181服务的状态
 * @param {Object} state
 */
export function changeStatus(){
  return request({
    url:'/v1/service/changeStatus',
    method:'post'
  })
}
/**
 * 获取gb28181状态
 * @param {Object} state
 */
export function getStatus(state){
  return request({
    url:'/v1/service/getStatus',
    method:'get'
  })
}
