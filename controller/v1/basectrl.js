const fs   = require('fs');
const http = require('http');
const constants = require('../../data/constants');

/**
 * 常量设置
 */
module.exports = {
    /**
     * 保存登陆信息
     */
    'tokens': {},
    'hasLogin': function(req, res){
        let token = req.header('X-Token');
        if(token){
            let admin = this.tokens[token];
            if(admin){
                return true;
            }else{
                res.send(Object.assign(constants.httpCode.ILLEGAL_TOKEN, {data:{}}));
                return false;
            }
        }
        else{
            res.send(Object.assign(constants.httpCode.ILLEGAL_TOKEN, {data:{}}));
            return false;
        }
    }
}