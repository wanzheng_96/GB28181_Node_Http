const log4js      = require('../../data/log');
const logger      = log4js.getLogger('info');
const constants   = require('../../data/constants');
const timingModel  = require('../../model/timing');
const format      = require('string-format');
const basectrl    = require('./basectrl');
const _ = require('lodash');

var path    = require("path");
var fs      = require("fs");
/**
 * 定时截图信息
 */
let Timing = {
   
    /**
     * 获取报警信息表
     * @param {*} req 
     * @param {*} res 
     * @returns 
     */
    list:async function(req,res){
        if(!basectrl.hasLogin(req, res)){
            return;
        }
        let page = req.query.page;
        let pagesize = req.query.pagesize;
        
        let deviceid = req.query.deviceid;
        let channelid = req.query.channelid;
  
        if(typeof(page) == 'undefined') page=1;
        if(!pagesize) { pagesize=10000; }
        if(typeof(deviceid) == 'undefined') {
            deviceid =null;
            channelid=null;
        }
  
        let count = await timingModel.count(deviceid, channelid);
        timingModel.list(page-1, pagesize, deviceid, channelid).then((rows)=>{
            let ret =Object.assign(constants.httpCode.OK , {data: {items: rows, total: count, page: page, pagesize: pagesize}});
            res.send(ret);
        });
        
    }
};
module.exports=Timing;
