const constants = require('../../data/constants');
const settingModel = require('../../model/setting');
const sipServer = require('../../sip/server');
const basectrl    = require('./basectrl');
/**
 * 系统设置项
 */
let Setting={
    /**
     * 加载设置
     * @param {*} req 
     * @param {*} res 
     */
    load:function(req,res){
        if(!basectrl.hasLogin(req, res)){
            return;
        }
        settingModel.load().then(function(row){
            let data = Object.assign(constants.httpCode.OK,{data:row});
            res.send(data);
        })
    },
    save:function(req,res){
        if(!basectrl.hasLogin(req, res)){
            return;
        }
        settingModel.save(req.body).then(function(row){
            let data = Object.assign(constants.httpCode.OK,{data:row})
            res.send(data)
            // 重启sipServer
            settingModel.load().then((sipSetting)=>{
                if(sipSetting.sip_rtp_host){
                /**
                 * 启动gb28181服务
                 */
                    sipServer.restart(sipSetting);
                }
            });
        })
    }
};

module.exports=Setting;