const constants   = require('../../data/constants');
const mediaServerModel = require('../../model/mediaserver');
const log4js      = require('../../data/log');
const logger      = log4js.getLogger('info');
const basectrl    = require('./basectrl');

/**
 * 媒体服务器信息管理
 */

 let MediaServer={
    /**
     * 默认使用本地作为媒体服务器
     */
    init : async function(){
        let mediaServerCount = await mediaServerModel.count();
        let ip = await constants.publicIp();
        if(mediaServerCount == 0){
            await mediaServerModel.insert({
                server_id   : 'NQZSziqcnLJjV65M',
                media_host  : ip,
                http_port   : 9094,
                https_port  : 1443,
                rtmp_port   : 9092,
                rtp_min_port : 50000,
                rtp_max_port : 50500,
                secret      : '035c73f7-bb6b-4889-a715-d9eb2d1925cc'
            });
        }
    },
    /**
     * API接口，获取设备列表
     * @param {*} req 
     * @param {*} res 
     */
    list: async function (req, res){
        if(!basectrl.hasLogin(req, res)){
            return;
        }        
        let page = req.query.page;
        let pagesize = req.query.pagesize;
        if(typeof(page)=='undefined') page=1;
        if(!pagesize) pagesize=10000;
  
        let count = await mediaServerModel.count();
        mediaServerModel.list(page-1, pagesize).then((rows)=>{
            for(let i in rows){
                delete rows[i].secret ;
            }
            let ret =Object.assign(constants.httpCode.OK , {data:{items:rows, total:count, page:page, pagesize:pagesize}});
            res.send(ret);
        }).catch((err) => {
            logger.error(err);
        });
    },

    /**
     * Query接口读取一个媒体服务器信息
     * @param {*} req 
     * @param {*} res 
     */
    query:function(req,res){
        if(!basectrl.hasLogin(req, res)){
            return;
        }
        let server_id = req.query.server_id;

        mediaServerModel.getByServerId(server_id,(ret)=>{
            if(ret.length){
                let send =Object.assign(constants.httpCode.OK , {data:{items:ret,total:ret.length}});
                res.send(send);
            }else{
                let send =Object.assign(constants.httpCode.NO_DATA , {data:null});
                res.send(send);
            }
        });
    },
    /**
     * 更新媒体服务器信息
     */
    update:function(req,res){
        if(!basectrl.hasLogin(req, res)){
            return;
        }
        mediaServerModel.update(req.body);
        let ret =Object.assign(constants.httpCode.OK , {data:{}});
        res.send(ret);
    },
    /**
     * 插入一个媒体服务器信息
     */
    insert:function(req,res){
        if(!basectrl.hasLogin(req, res)){
            return;
        }
        // 如果已经存在，返回错误
        mediaServerModel.getByServerId(req.body.server_id, (item)=>{
            if(item && item.length>0){
                let send =Object.assign({}, constants.httpCode.DUPLITED);
                res.send(send);
            }else{
                mediaServerModel.insert(req.body).then((ret) => {
                    logger.info('ret',ret)
                    let send =Object.assign({id: ret.id}, constants.httpCode.OK);
                    res.send(send);
                });
            }
        })
    },
    /**
     * 删除一个媒体服务器信息
     */
    del:function(req,res){
        if(!basectrl.hasLogin(req, res)){
            return;
        }
        mediaServerModel.del(req.body.server_id);

        let ret =Object.assign(constants.httpCode.OK , {data:{}});
        res.send(ret);
    },

 };
 module.exports=MediaServer;