const constants  = require("../../data/constants");
const adminModel = require('../../model/admin');
var UUID         = require('uuid');
const log4js     = require('../../data/log');
const logger     = log4js.getLogger('info');
const basectrl   = require('./basectrl');

let admin  = {};

admin.init = async() => {
  // 如果没有管理员信息就新建
  let adminAccount = await adminModel.count();
  if(adminAccount == 0){
    await adminModel.insert(constants.defaultAdmin.username, constants.defaultAdmin.password);
  }
}
/**
  登陆
 * @param {*} req 
 * @param {*} res 
 */
admin.checklogin=async(req,res)=>{
    
    let username = req.body.username;
    let password = req.body.password;
    if(!username || !password){
        res.send(constants.httpCode.ILLEGAL_PARAM);
    }else{
        await adminModel.checklogin(username, password).then((data)=>{
            if(data && data.length>0){
                let token = UUID.v1().replace(/-/g,'');
                basectrl.tokens[token] = {
                    token: data,
                    name: username
                };
                res.send( Object.assign(constants.httpCode.OK , {
                    data:{
                        token: token,
                        avatar: 'assets/images/user.png',
                        name: username
                    }
                }));
            }else{
                res.send(Object.assign(constants.httpCode.PASS_ERROR, {data:{}}));
            }
            
        }).catch((err)=>{
            logger.error(err)
            res.send(constants.httpCode.ILLEGAL_PARAM);
        });
    }
}

// 注销
admin.logout=function(req,res){
    let token = req.header('X-Token');
    if(basectrl.tokens[token]){
        res.send(Object.assign(constants.httpCode.OK,{data:{}}));
    }
    else{
        res.send(Object.assign(constants.httpCode.ILLEGAL_TOKEN, {data:{}}));
    }
    
}

// 用户信息
admin.info=function(req,res){
    let token = req.query.token;
    if(basectrl.tokens[token]){
        res.send(Object.assign(constants.httpCode.OK,{data:{
            token:token,
            avatar: 'assets/images/user.png',
            name: basectrl.tokens[token].name
        }}));
    }else{
        res.send(Object.assign(constants.httpCode.ILLEGAL_TOKEN, {data:{}}));
    }
}
admin.changePass=function(req,res){
    let token = req.header('X-Token');
    logger.info(basectrl.tokens,'token=', token);
    logger.info(basectrl.tokens[token]);
    if(basectrl.tokens[token]){
        let name = basectrl.tokens[token].name;
        let old_pass=req.body.old_pass;
        let new_pass=req.body.new_pass;
        adminModel.changePass(name,old_pass,new_pass).then(()=>{
            res.send(Object.assign(constants.httpCode.OK,{data:{
                token:token,
                avatar: 'assets/images/user.png',
                name: basectrl.tokens[token].name
            }}));
        }).catch(()=>{
            res.send(Object.assign(constants.httpCode.PASS_ERROR, {data:{}}));    
        })
    }else{
        res.send(Object.assign(constants.httpCode.ILLEGAL_TOKEN, {data:{}}));
    }
}

module.exports=admin;