const log4js      = require('../../data/log');
const logger      = log4js.getLogger('info');
const constants   = require('../../data/constants');
const deviceModel = require('../../model/device');
const channelModel= require('../../model/channel');
const sipServer   = require('../../sip/server');
const basectrl    = require('./basectrl');

let Device={

    /**
     * API接口，获取设备列表
     * @param {*} req 
     * @param {*} res 
     */
    list: async function (req, res){
      if(!basectrl.hasLogin(req, res)){
        return;
      }
      let page = req.query.page;
      let pagesize = req.query.pagesize;
      let device = req.query.device;

      if(typeof(page) == 'undefined') page=1;
      if(!pagesize) { pagesize=10000; }
      if(typeof(device) == 'undefined') {
        device=null;
      }

      let count = await deviceModel.count(device);
      deviceModel.list(page-1, pagesize, device).then((rows)=>{
          let ret =Object.assign(constants.httpCode.OK , {data: {items: rows, total: count, page: page, pagesize: pagesize}});
          res.send(ret);
      });
    },
    /**
     * Query接口读取一个摄像头信息
     * @param {*} req 
     * @param {*} res 
     */
    query:async function(req,res){
      if(!basectrl.hasLogin(req, res)){
        return;
      }
      let deviceid = req.query.deviceid;
      let ret = await deviceModel.getDeviceByDeviceId(deviceid);

      if(ret.length){
        let send =Object.assign(constants.httpCode.OK , {data:{items:ret,total:ret.length}});
        res.send(send);
      }else{
        let send =Object.assign(constants.httpCode.NO_DATA , {data:null});
        res.send(send);
      }
    },
    /**
     * 请求目录
     * @param {*} req 
     * @param {*} res 
     */
    catalog:function(req,res){
      if(!basectrl.hasLogin(req, res)){
        return;
      }
      let account = req.query.account;
      sipServer.catalog(account);
      let ret =Object.assign(constants.httpCode.OK , {data:{}});
      res.send(ret);
    },
    deldevice:function(req,res){
      if(!basectrl.hasLogin(req, res)){
        return;
      }
      deviceModel.del(req.body.account);
      // 删除缓存信息
      if(constants.registry && constants.registry[req.body.account]){
        delete constants.registry[req.body.account];
      }
      let ret =Object.assign(constants.httpCode.OK , {data:{}});
      res.send(ret);
    },
    /**
     * 更新通道名称
     * @param {*} req 
     * @param {*} res 
     */
    updatename:function(req,res){
      if(!basectrl.hasLogin(req, res)){
        return;
      }
      deviceModel.update({
        deviceid: req.body.deviceid,
        device_name: req.body.device_name
      });
      let ret =Object.assign(constants.httpCode.OK , {data:{}});
      res.send(ret);
    },
    /**
     * 更新
     * @param {*} req 
     * @param {*} res 
     */
    update: async function(req, res){
      if(!basectrl.hasLogin(req, res)){
        return;
      }

      let deviceInDb =await deviceModel.getDeviceByDeviceId(req.body.deviceid);
      if(deviceInDb.protocol != req.body.protocol){
        // 协议变更，删除原来的子通道信息
        channelModel.deleteByDeviceId(req.body.deviceid);
      }

      deviceModel.update({
        deviceid: req.body.deviceid,
        server_id: req.body.server_id,
        device_name: req.body.device_name,
        rtsp_url: req.body.rtsp_url,
        protocol: req.body.protocol,
        sumnum: req.body.sumnum
      });

      let channelCount = req.body.sumnum;
        
      if(req.body.protocol=='RTSP拉流'){
        if(req.body.rtsp_url){ 
          for(let i=0;i<channelCount;i++){
            let rtsp = req.body.rtsp_url.replace('{channel}', (i+1));
            channelModel.updateDeviceInfo({
              DeviceID: (i+1),
              ParentID: req.body.deviceid,
              Name: '',
              Manufacturer: '', Model:'', Owner:'',CivilCode:'', Address:'', RegisterWay:'', Secrecy:'',
              rtsp_url: rtsp
            });
          }
        }
      }else{
        // 等待国标catalog
      }
      
      let ret =Object.assign(constants.httpCode.OK , {data:{}});
      res.send(ret);
    }
};

module.exports=Device;