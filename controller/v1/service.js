const constants     = require("../../data/constants");
const sipServer     = require('../../sip/server');
const log4js        = require('../../data/log');
const logger        = log4js.getLogger('request');
const cpuStat       = require('cpu-stat');
const osutils       = require('os-utils');
const basectrl    = require('./basectrl');

let Service={};

/**
 * 改变Service服务的状态
 * @param {*} req 
 * @param {*} res 
 */
Service.changeStatus=function(req,res){
    if(!basectrl.hasLogin(req, res)){
        return;
    }
    if(sipServer.running){
        sipServer.stop();
    }else{
        sipServer.start();
    }
    let ret=Object.assign(constants.httpCode.OK,{
        data:{"running": sipServer.running}
    });
    res.send(ret);
}
/**
 * 获取Service服务的状态
 * @param {*} req 
 * @param {*} res 
 */
Service.getStatus=function(req,res){
    if(!basectrl.hasLogin(req, res)){
        return;
    }
    const mem = 100*(osutils.totalmem()-osutils.freemem())/osutils.totalmem();

    cpuStat.usagePercent(function(err, percent, seconds) {
       if (err) {
           return console.log(err);
       }
       let ret=Object.assign(constants.httpCode.OK,{
            data: {
                running: sipServer.running,
                start: constants.start,
                cpu_usage: percent,
                mem_usage: mem,
                mem_total: osutils.totalmem(),
                mem_free : osutils.freemem()
            }
        });
    res.send(ret);
   });
}
/**
 * 获取连接的Service客户端
 * @param {*} req 
 * @param {*} res 
 */
Service.getClients=function(req,res){
    if(!basectrl.hasLogin(req, res)){
        return;
    }
    let ret=Object.assign(constants.httpCode.OK,{
        data:constants.registry
    });
    res.send(ret);
}
module.exports=Service;